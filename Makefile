RASPI = /home/marcin/workspace/raspi

CC = $(RASPI)/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc
CFLAGS = --sysroot $(RASPI)/sysroot
LDFLAGS = -l bcm2835

all: dot_test

dot_test: dot_test.o
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)

clean:
	$(RM) -rf *.o dot_test

deploy_nanopi:
	scp dot_test dot_test.py root@10.42.0.189:dot_test/

deploy_raspi:
	scp dot_test dot_test.py pi@10.42.0.240:dot_test/
